# ITRockstars - Music app

A Java application built with Java Spring Boot and Maven. 

The Application loads in 2 files at runtime and exposes endpoints on port 8080 to the outside for accessing the data

# Running the project

Compiling the project

`mvn clean package`

Running the project on windows

`java -jar target\itrockstars-0.0.1-SNAPSHOT.jar`

Running the project on unix based system

`java -jar target/itrockstars-0.0.1-SNAPSHOT.jar`

# Todo list
Due to time constraints in the assignment quite a bit of functionality was not implemented yet. If I had more time I would have wanted to implement the following:

* REST API part for the artists
* Unit tests for the artists api endpoint
* More unhappy flow unit testing (eg. updating non existing artists/songs, invalid input testing)
* Input validation on the api endpoints
* Filtering out artists that do not have a song in the Metal genre