package com.jonathan.itrockstars.controllers;

import com.jonathan.itrockstars.models.SongEntity;
import com.jonathan.itrockstars.repositories.SongRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SongControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SongRepository songRepository;

    /**
     * Tests song creation
     * @throws Exception
     */
    @Test
    public void testCreateSong() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = post("/song")
                .param("Artist", "jonathan")
                .param("Name", "jonathan song")
                .param("Duration", "10000")
                .param("Year", "2010")
                .param("Bpm", "150")
                .param("Genre", "Metal")
                .param("SpotifyId", "jonathan spotify id")
                .param("ShortName", "jonasong")
                .param("Album", "jonathan album");

        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(status().isOk());

        SongEntity song = songRepository.findByName("jonathan song");
        assertNotNull(song);
        assertEquals("jonathan spotify id", song.getSpotifyId());
    }

    /**
     * Tests if the song endpoint returns a 200 status
     * @throws Exception
     */
    @Test
    public void testReadAllSongs() throws Exception {
        mockMvc.perform(get("/song"))
                .andExpect(status().isOk());
    }

    /**
     * Tests if an existing song can be updated
     * @throws Exception
     */
    @Test
    public void testUpdateSong() throws Exception {
        SongEntity savedSong = createAndSaveSong();

        MockHttpServletRequestBuilder requestBuilder = put("/song")
                .param("Id", Long.toString(savedSong.getId()))
                .param("Name", "updated new song")
                .param("Year", "1995")
                .param("Artist", "jonathan")
                .param("ShortName", "nsong")
                .param("Bpm", "200")
                .param("Duration", "34234")
                .param("Genre", "Metal")
                .param("SpotifyId", "new spotify id")
                .param("Album", "jonathan album");

        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(status().isOk());

        Optional<SongEntity> optionalUpdatedSong = songRepository.findById(savedSong.getId());
        assertTrue(optionalUpdatedSong.isPresent());
        assertEquals("updated new song", optionalUpdatedSong.get().getName());
    }

    /**
     * Tests if an existing song can be deleted
     * @throws Exception
     */
    @Test
    public void deleteSongExists() throws Exception {
        SongEntity songToDelete = createAndSaveSong();
        MockHttpServletRequestBuilder requestBuilder = delete("/song/" + songToDelete.getId());

        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(status().isOk());

        Optional<SongEntity> optionalUpdatedSong = songRepository.findById(songToDelete.getId());
        assertFalse(optionalUpdatedSong.isPresent());
    }

    /**
     * Tests removing a non existing song results in a 404
     * @throws Exception
     */
    @Test
    public void deleteSongDoesntExist() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = delete("/song/" + -1L);

        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(status().isNotFound());
    }

    /**
     * Tests reading a single song
     */
    @Test
    public void testReadSong() {

    }

    /**
     * Tests finding a list of songs by genre
     */
    @Test
    public void testFindByGenre() {

    }

    /**
     * Creates, saves and returns a SongEntity for testing purposes
     * @return SongEntity
     */
    private SongEntity createAndSaveSong() {
        SongEntity newSong = new SongEntity();
        newSong.setName("new song");
        newSong.setYear(1995);
        newSong.setArtist("jonathan");
        newSong.setShortName("nsong");
        newSong.setBpm(200);
        newSong.setDuration(34234);
        newSong.setGenre("Metal");
        newSong.setSpotifyId("new spotify id");
        newSong.setAlbum("jonathan album");

        return songRepository.save(newSong);
    }
}
