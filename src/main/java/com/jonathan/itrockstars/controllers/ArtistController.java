package com.jonathan.itrockstars.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/artist")
public class ArtistController {
    /**
     * Creates and saves an artist to the database
     */
    @PostMapping("")
    public void create() {

    }

    /**
     * Updates an existing artist and saves it to the database
     */
    @PutMapping("")
    public void update() {

    }

    /**
     * Removes an existing artist from the database
     */
    @DeleteMapping("/{id}")
    public void delete() {

    }

    /**
     * Returns a list of all existing artists in the database
     */
    @GetMapping("")
    public void readAll() {

    }

    /**
     * Finds and returns an artist based on ID
     */
    @GetMapping("/{id}")
    public void read() {

    }

    /**
     * Find and returns an artist based on name
     */
    @GetMapping("/find/{name")
    public void findByName() {

    }
}
