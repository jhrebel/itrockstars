package com.jonathan.itrockstars.controllers;

import com.jonathan.itrockstars.models.SongEntity;
import com.jonathan.itrockstars.repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("song")
public class SongController {
    private final SongRepository songRepository;

    public SongController(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    /**
     * Create a new song and saves it to the database layer
     * @param Bpm
     * @param Year
     * @param ShortName
     * @param Album
     * @param Name
     * @param Artist
     * @param Duration
     * @param SpotifyId
     * @return SongEntity
     */
    @PostMapping("")
    public SongEntity create(
            @RequestParam Long Bpm,
            @RequestParam Long Year,
            @RequestParam String ShortName,
            @RequestParam String Album,
            @RequestParam String Name,
            @RequestParam String Artist,
            @RequestParam Long Duration,
            @RequestParam String SpotifyId
    ) {
        SongEntity songEntity = new SongEntity();
        songEntity.setArtist(Artist);
        songEntity.setName(Name);
        songEntity.setShortName(ShortName);
        songEntity.setDuration(Duration);
        songEntity.setBpm(Bpm);
        songEntity.setAlbum(Album);
        songEntity.setYear(Year);
        songEntity.setSpotifyId(SpotifyId);

        return songRepository.save(songEntity);
    }

    /**
     * Updates a song and saves it to the database layer
     * @param Id
     * @param Bpm
     * @param Year
     * @param ShortName
     * @param Album
     * @param Name
     * @param Artist
     * @param Duration
     * @param SpotifyId
     * @return SongEntity
     */
    @PutMapping("")
    public SongEntity update(
            @RequestParam Long Id,
            @RequestParam Long Bpm,
            @RequestParam Long Year,
            @RequestParam String ShortName,
            @RequestParam String Album,
            @RequestParam String Name,
            @RequestParam String Artist,
            @RequestParam Long Duration,
            @RequestParam String SpotifyId
    ) {
        SongEntity songEntity = new SongEntity();
        songEntity.setId(Id);
        songEntity.setArtist(Artist);
        songEntity.setName(Name);
        songEntity.setShortName(ShortName);
        songEntity.setDuration(Duration);
        songEntity.setBpm(Bpm);
        songEntity.setAlbum(Album);
        songEntity.setYear(Year);
        songEntity.setSpotifyId(SpotifyId);

        return songRepository.save(songEntity);
    }

    /**
     * Delete a song from the database based on the song id
     * @param id
     * @return String
     * @throws ResponseStatusException
     */
    @DeleteMapping("/{id}")
    public String delete(
            @PathVariable Long id
    ) throws ResponseStatusException {
        Optional<SongEntity> songToDelete = songRepository.findById(id);
        if (songToDelete.isPresent()) {
            songRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Song was not found");
        }

        return "Song deleted";
    }

    /**
     * Returns a list of all songs in the database
     * @return List<SongEntity>
     */
    @GetMapping("")
    public List<SongEntity> readAll() {
        return songRepository.findAll();
    }

    /**
     * Returns a song based on the id
     * @return SongEntity
     * @throws ResponseStatusException
     */
    @GetMapping("/{id}")
    public SongEntity read() throws ResponseStatusException {
        return new SongEntity();
    }

    /**
     * Returns a list of songs
     * @return List<SongEntity>
     */
    @GetMapping("/findByGenre/{genre}")
    public List<SongEntity> findByGenre() {
        return new ArrayList<>();
    }
}
