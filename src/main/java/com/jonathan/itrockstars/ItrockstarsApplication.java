package com.jonathan.itrockstars;

import com.jonathan.itrockstars.models.ArtistEntity;
import com.jonathan.itrockstars.models.SongEntity;
import com.jonathan.itrockstars.repositories.ArtistRepository;
import com.jonathan.itrockstars.repositories.SongRepository;
import com.jonathan.itrockstars.services.JsonFileReaderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@Log4j2
public class ItrockstarsApplication implements ApplicationRunner {
	private final JsonFileReaderService jsonFileReaderService;
	private final SongRepository songRepository;
	private final ArtistRepository artistRepository;

	public ItrockstarsApplication(JsonFileReaderService jsonFileReaderService,
								  SongRepository songRepository,
								  ArtistRepository artistRepository) {
		this.jsonFileReaderService = jsonFileReaderService;
		this.songRepository = songRepository;
		this.artistRepository = artistRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(ItrockstarsApplication.class, args);
	}

    /**
     * Run does an import of the songs and the data files when starting the application
     * @param args
     */
	@Override
	public void run (ApplicationArguments args)
	{
		List<ArtistEntity> artists = jsonFileReaderService.readArtistFile();
		List<SongEntity> songs = jsonFileReaderService.readSongFile();

		artistRepository.saveAll(artists);
		songRepository.saveAll(songs);
	}
}
