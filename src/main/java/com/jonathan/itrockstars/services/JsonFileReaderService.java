package com.jonathan.itrockstars.services;

import com.jonathan.itrockstars.models.ArtistEntity;
import com.jonathan.itrockstars.models.SongEntity;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Log4j2
public class JsonFileReaderService {
    /**
     * readFile takes the filename of a filename stored in the resources and returns a JSONArray
     * @param filename
     * @return JSONArray
     */
    private JSONArray readFile(String filename) {
        JSONParser jsonParser = new JSONParser();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(filename);
        try(InputStreamReader reader = new InputStreamReader(is)) {
            return (JSONArray) jsonParser.parse(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONArray();
    }

    /**
     * readArtistFile reads in the file and returns a list of artistEntities
     * @return List<ArtistEntities> songs
     */
    public List<ArtistEntity> readArtistFile() {
        JSONArray artistObjects = readFile("artists.json");
        Stream<ArtistEntity> artistsStream = artistObjects.stream()
                .map( obj -> parseArtistObject((JSONObject) obj));
        List<ArtistEntity> artists = artistsStream.collect(Collectors.toList());

        return artists;
    }

    /**
     * readSongFile reads in the file and returns a list of songEntities
     * @return List<SongEntity> songs
     */
    public List<SongEntity> readSongFile() {
        JSONArray songObjects = readFile("songs.json");
        Stream<SongEntity> songsStream = songObjects.stream()
                .map( obj -> parseSongObject((JSONObject) obj))
                .filter( x -> ((SongEntity) x).getYear() < 2016L && ((SongEntity) x).getGenre().contains("Metal"));

        List<SongEntity> songs = songsStream.collect(Collectors.toList());

        return songs;
    }

    /**
     * parseSongObject parses the jsonObject and returns a SongEntity Object
     * @param JSONObject song
     * @return SongEntity
     */
    public SongEntity parseSongObject(JSONObject song) {
        return new SongEntity(
            (Long) song.get("Id"),
            (String) song.get("Name"),
            (Long) song.get("Year"),
            (String) song.get("Artist"),
            (String) song.get("Shortname"),
            (Long) (song.get("Bpm") != null ? song.get("Bpm") : 0L),
            (Long) song.get("Duration"),
            (String) song.get("Genre"),
            (String) (song.get("SpotifyId") != null ? song.get("SpotifyId") : ""),
            (String) song.get("Album")
        );
    }

    /**
     * parseArtistObject parses the jsonObject and returns a ArtistEntity Object
     * @param artist
     * @return
     */
    public ArtistEntity parseArtistObject(JSONObject artist) {
        return new ArtistEntity(
                (Long) artist.get("Id"),
                (String) artist.get("Name")
        );
    }
}
