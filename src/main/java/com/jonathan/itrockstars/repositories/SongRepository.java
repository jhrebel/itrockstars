package com.jonathan.itrockstars.repositories;

import com.jonathan.itrockstars.models.SongEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongRepository extends JpaRepository<SongEntity, Long> {
    /**
     * Finds a SongEntity based on the name
     * @param name
     * @return SongEntity
     */
    SongEntity findByName(String name);
}
