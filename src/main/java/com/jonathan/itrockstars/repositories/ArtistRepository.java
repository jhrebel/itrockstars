package com.jonathan.itrockstars.repositories;

import com.jonathan.itrockstars.models.ArtistEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ArtistRepository extends JpaRepository<ArtistEntity, Long> {
}
